
#ifndef BOOKINGINGENERAL_H
#define BOOKINGINGENERAL_H
#include "general.h"

class BookingInGeneral {
public:
    
    BookingInGeneral();
    BookingInGeneral(long id, string price, string fromDate, string toDate, string tripID,string customerID,string customerName);
    long getID();
    static long getCurrentID();
    virtual ~BookingInGeneral();
protected:
    static long currentID;
    long id;
    double price;
    long tripID;
    long customerID;
    string customerName;
    string fromDate;
    string toDate;
    
};

#endif /* BOOKINGINGENERAL_H */

