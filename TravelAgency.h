

#ifndef TRAVELAGENCY_H
#define TRAVELAGENCY_H

#include "FlightBooking.h"
#include "HotelBooking.h"
#include "CarReservation.h"
#include "general.h"
#include "BookingInGeneral.h"
#include "Trip.h"
#include "Customer.h"

    const int asciiF=70;
    const int asciiH=72;
    const int asciiR=82;
    
class TravelAgency {
public:
    TravelAgency();
    void readfile();
    BookingInGeneral* findBooking(long id);
    Trip* findTrip (long id);
    Customer* findCustomer(long id);
    int createBooking(char, string,string,string,string,vector<string>);
    virtual ~TravelAgency();
private:
    vector<BookingInGeneral*> allBookings;
    vector<Customer*> allCustomers;
    vector<Trip*> allTrips;
};

#endif /* TRAVELAGENCY_H */

