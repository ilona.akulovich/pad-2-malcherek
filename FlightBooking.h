
#ifndef FLIGHTBOOKING_H
#define FLIGHTBOOKING_H
#include "general.h"
#include "BookingInGeneral.h"
class FlightBooking: public BookingInGeneral {
public:
    FlightBooking(long,string,string,string,string,string,string,string,string,string);
    void print();
    virtual ~FlightBooking();
private:
//    long id;
//    double price;
//    string fromDate;
//    string toDate;
    string fromDest;
    string toDest;
    string airline;
};

#endif /* FLIGHTBOOKING_H */

