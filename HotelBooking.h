
#ifndef HOTELBOOKING_H
#define HOTELBOOKING_H
#include "general.h"
#include "BookingInGeneral.h"
class HotelBooking:public BookingInGeneral{
public:
    HotelBooking(long,string,string,string,string,string,string,string,string);
    void print();
    virtual ~HotelBooking();
private:
//    long id;
//    double price;
//    string fromDate;
//    string toDate;
    string hotel;
    string town;
};

#endif /* HOTELBOOKING_H */

