

#include "Trip.h"

Trip::Trip(string tripID,string customerID) {
    this->id=stoi(tripID);
    this->customerID=stoi(customerID);
}

void Trip::addBooking(BookingInGeneral* theBooking){
    relatedBookings.push_back(theBooking);
}
long Trip::getID(){
    return this->id;
}
long Trip::getCustomerID(){
    return this->customerID;
}
void Trip::printBookings(){
    cout<<"Trip with id "<<id<<" has "<<relatedBookings.size()<<" bookings"<<endl<<flush;
}

Trip::~Trip() {
}

