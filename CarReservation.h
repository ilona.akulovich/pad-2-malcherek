

#ifndef CARRESERVATION_H
#define CARRESERVATION_H
#include "general.h"
#include "BookingInGeneral.h"
class CarReservation : public BookingInGeneral{
public:
    CarReservation(long,string,string,string,string,string,string,string,string,string);
    void print();
    virtual ~CarReservation();
private:
//    long id;
//    double price;
//    string fromDate;
//    string toDate;
    string pickupLocation;
    string returnLocation;
    string company;
};

#endif /* CARRESERVATION_H */

