

#include "FlightBooking.h"

FlightBooking::FlightBooking(long id,string price,
                            string fromDate,string toDate, 
                            string tripID,string customerID,string customerName,
                            string fromDest,string toDest,string airline) 

:BookingInGeneral(id,price,fromDate,toDate,tripID,customerID,customerName)//call BookingIG constructor for id
{
    this->fromDest=fromDest;
    this->toDest=toDest;
    this->airline=airline;
}

FlightBooking::~FlightBooking() {
}

void FlightBooking::print(){
    cout<<"FlightBooking "<<id<<endl;
}