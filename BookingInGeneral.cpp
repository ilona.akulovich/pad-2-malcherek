
#include "BookingInGeneral.h"


BookingInGeneral::BookingInGeneral() {
    if (currentID<=this->id){
        currentID=this->id++;}
}
BookingInGeneral::BookingInGeneral(long id, string price, string fromDate, string toDate, string tripID,string customerID,string customerName) {
    this->id=id;
    this->price=stoi(price);
    this->fromDate=fromDate;
    this->toDate=toDate;
    this->tripID=stoi(tripID);
    this->customerID=stoi(customerID);
    this->customerName=customerName;
    
    if (currentID<=this->id){
        currentID=this->id+1;
        }
}
long BookingInGeneral::getID(){
    return this->id;
    
}
 long BookingInGeneral::getCurrentID(){
    return BookingInGeneral::currentID;
}

BookingInGeneral::~BookingInGeneral() {
}

long BookingInGeneral::currentID = 1;